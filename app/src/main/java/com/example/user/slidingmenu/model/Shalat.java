package com.example.user.slidingmenu.model;

import java.util.Calendar;

/**
 * Created by raizal.pregnanta on 06/10/2016.
 */
public class Shalat {

    protected String Fajr;
    protected String Sunrise;
    protected String Dhuhr;
    protected String Asr;
    protected String Sunset;
    protected String Maghrib;
    protected String Isha;
    protected String Imsak;
    protected String Midnight;

    protected long timestamp = 0;

    public Shalat(long timestamp, String fajr, String sunrise, String dhuhr, String asr, String maghrib, String isha, String imsak, String midnight) {
        Fajr = fajr;
        Sunrise = sunrise;
        Dhuhr = dhuhr;
        Asr = asr;
        Maghrib = maghrib;
        Isha = isha;
        Imsak = imsak;
        Midnight = midnight;
        this.timestamp = timestamp;
    }

    public long getFajrTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        String[] time = getFajr();
        calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(time[0]));
        calendar.set(Calendar.MINUTE, Integer.valueOf(time[1]));
        return calendar.getTimeInMillis();
    }

    public long getSunriseTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        String[] time = getSunrise();
        calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(time[0]));
        calendar.set(Calendar.MINUTE, Integer.valueOf(time[1]));
        return calendar.getTimeInMillis();
    }

    public long getDhuhrTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        String[] time = getDhuhr();
        calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(time[0]));
        calendar.set(Calendar.MINUTE, Integer.valueOf(time[1]));
        return calendar.getTimeInMillis();
    }

    public long getAsrTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        String[] time = getAsr();
        calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(time[0]));
        calendar.set(Calendar.MINUTE, Integer.valueOf(time[1]));
        return calendar.getTimeInMillis();
    }

    public long getSunsetTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        String[] time = getSunset();
        calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(time[0]));
        calendar.set(Calendar.MINUTE, Integer.valueOf(time[1]));
        return calendar.getTimeInMillis();
    }

    public long getMaghribTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        String[] time = getMaghrib();
        calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(time[0]));
        calendar.set(Calendar.MINUTE, Integer.valueOf(time[1]));
        return calendar.getTimeInMillis();
    }

    public long getIshaTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        String[] time = getIsha();
        calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(time[0]));
        calendar.set(Calendar.MINUTE, Integer.valueOf(time[1]));
        return calendar.getTimeInMillis();
    }

    public long getImsakTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        String[] time = getImsak();
        calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(time[0]));
        calendar.set(Calendar.MINUTE, Integer.valueOf(time[1]));
        return calendar.getTimeInMillis();
    }

    public long getMidnightTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        String[] time = getMidnight();
        calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(time[0]));
        calendar.set(Calendar.MINUTE, Integer.valueOf(time[1]));
        return calendar.getTimeInMillis();
    }

    public String[] getFajr() {
        return Fajr.replaceAll(" \\(WIB\\)", "").split(":");
    }

    public String[] getSunrise() {
        return Sunrise.replaceAll(" \\(WIB\\)", "").split(":");
    }

    public String[] getDhuhr() {
        return Dhuhr.replaceAll(" \\(WIB\\)", "").split(":");
    }

    public String[] getAsr() {
        return Asr.replaceAll(" \\(WIB\\)", "").split(":");
    }

    public String[] getSunset() {
        return Sunset.replaceAll(" \\(WIB\\)", "").split(":");
    }

    public String[] getMaghrib() {
        return Maghrib.replaceAll(" \\(WIB\\)", "").split(":");
    }

    public String[] getIsha() {
        return Isha.replaceAll(" \\(WIB\\)", "").split(":");
    }

    public String[] getImsak() {
        return Imsak.replaceAll(" \\(WIB\\)", "").split(":");
    }

    public String[] getMidnight() {
        return Midnight.replaceAll(" \\(WIB\\)", "").split(":");
    }

    //
    public String getFajrString() {
        return Fajr;
    }

    public String getSunriseString() {
        return Sunrise;
    }

    public String getDhuhrString() {
        return Dhuhr;
    }

    public String getAsrString() {
        return Asr;
    }

    public String getSunsetString() {
        return Sunset;
    }

    public String getMaghribString() {
        return Maghrib;
    }

    public String getIshaString() {
        return Isha;
    }

    public String getImsakString() {
        return Imsak;
    }

    public String getMidnightString() {
        return Midnight;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
