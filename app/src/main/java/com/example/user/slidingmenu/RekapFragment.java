package com.example.user.slidingmenu;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alamkanak.weekview.MonthLoader;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;
import com.example.user.slidingmenu.model.Alarm;
import com.example.user.slidingmenu.model.AlarmDatabase;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class RekapFragment extends Fragment {

    @InjectView(R.id.weekView)
    WeekView weekView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rekap, container, false);
        ButterKnife.inject(this, view);

        weekView.setMonthChangeListener(new MonthLoader.MonthChangeListener() {
            @Override
            public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(Utils.extractDateOnly(calendar.getTimeInMillis()));
                calendar.set(Calendar.YEAR,newYear);
                calendar.set(Calendar.MONTH,newMonth);
                calendar.set(Calendar.DAY_OF_MONTH,1);

                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTimeInMillis(Utils.extractDateOnly(calendar2.getTimeInMillis()));
                calendar2.set(Calendar.YEAR,newYear);
                calendar2.set(Calendar.MONTH,newMonth);
                calendar2.set(Calendar.DAY_OF_MONTH,1);
                calendar2.add(Calendar.MONTH,1);
                calendar2.add(Calendar.DAY_OF_MONTH,-1);

                List<Alarm> alarms = Application.alarmDatabase.getDoneAlarm("time>="+calendar.getTimeInMillis()+" and time<="+calendar2.getTimeInMillis());

                List<WeekViewEvent> data = new ArrayList<WeekViewEvent>();

                for(Alarm alarm : alarms){
                    String status = "";

                    int color = Color.GREEN;

                    switch (AlarmDatabase.ALARM_STATUS.values()[alarm.getStatus()]){
                        case DELAY:
                            status = "ingatkan lagi 5 menit";
                            color = Color.BLUE;
                            break;
                        case SUDAH:
                            status = "dijalankan";
                            color = Color.GREEN;
                            break;
                        case ABAIKAN:
                            status = "diabaikan";
                            color = Color.RED;
                            break;
                    }

                    Calendar awal = Calendar.getInstance();
                    Calendar akhir = Calendar.getInstance();

                    awal.setTimeInMillis(alarm.getTime());
                    akhir.setTimeInMillis(alarm.getActionTime());

                    WeekViewEvent event = new WeekViewEvent(alarm.getId(),alarm.getText()+"\n"+status,awal,akhir);
                    event.setColor(color);
                    data.add(event);
                }

                return data;
            }
        });

        return view;

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }
}
