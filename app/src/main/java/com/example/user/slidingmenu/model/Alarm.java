package com.example.user.slidingmenu.model;

import com.example.user.slidingmenu.Utils;

/**
 * Created by raizal.pregnanta on 10/10/2016.
 */
public class Alarm {
    protected long id;
    protected long time;
    protected String text;
    protected int status = 0;
    protected int type;
    protected  long actionTime;

    public Alarm(long id, long time, String text, int status, int type, long actionTime) {
        this.id = id;
        this.time = time;
        this.text = text;
        this.status = status;
        this.type = type;
        this.actionTime = actionTime;
    }

    public Alarm(long id, long time, Utils.ALARM_TYPE type, String text) {
        this.id = id;
        this.time = time;
        this.text = text;
        this.type = type.ordinal();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getActionTime() {
        return actionTime;
    }

    public void setActionTime(long actionTime) {
        this.actionTime = actionTime;
    }
}
