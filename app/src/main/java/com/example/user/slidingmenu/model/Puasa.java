package com.example.user.slidingmenu.model;

/**
 * Created by raizal.pregnanta on 10/10/2016.
 */
public class Puasa {

    protected long id,date,time;
    protected int type;
    protected String name;

    public Puasa(long id,long date,String name,long time,int type){
        this.id = id;
        this.date = date;
        this.name = name;
        this.time = time;
        this.type = type;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
