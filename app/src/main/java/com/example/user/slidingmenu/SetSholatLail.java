package com.example.user.slidingmenu;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.user.slidingmenu.model.Shalat;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class SetSholatLail extends AppCompatActivity {

    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    
    @InjectView(R.id.btn_lail_ahad)
    CircleCheckBox btnLailAhad;
    @InjectView(R.id.btn_lail_senin)
    CircleCheckBox btnLailSenin;
    @InjectView(R.id.btn_lail_selasa)
    CircleCheckBox btnLailSelasa;
    @InjectView(R.id.btn_lail_rabu)
    CircleCheckBox btnLailRabu;
    @InjectView(R.id.btn_lail_kamis)
    CircleCheckBox btnLailKamis;
    @InjectView(R.id.btn_lail_jumat)
    CircleCheckBox btnLailJumat;
    @InjectView(R.id.btn_lail_sabtu)
    CircleCheckBox btnLailSabtu;
    @InjectView(R.id.linier_HariLail)
    LinearLayout linierHariLail;
    @InjectView(R.id.spinner_lail)
    Spinner spinnerLail;
    @InjectView(R.id.update_text_lail)
    TextView waktuSelanjutnya;
    private int mHour, mMinute;

    int res[] = new int[]{R.id.btn_lail_ahad, R.id.btn_lail_senin, R.id.btn_lail_selasa, R.id.btn_lail_rabu, R.id.btn_lail_kamis, R.id.btn_lail_jumat, R.id.btn_lail_sabtu};
    int days[] = new int[]{Calendar.SUNDAY, Calendar.MONDAY, Calendar.TUESDAY, Calendar.WEDNESDAY, Calendar.THURSDAY, Calendar.FRIDAY, Calendar.SATURDAY};
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_sholat_lail);
        ButterKnife.inject(this);
        setSupportActionBar(toolbar);
        
        for (int i = 0; i < res.length; i++) {
            ((CircleCheckBox) ButterKnife.findById(this, res[i])).setChecked(Const.malamHarian(days[i]));
        }
        
        spinnerLail.setSelection(Const.ringtone(Utils.ALARM_TYPE.SHOLAT_MALAM));

        List<Shalat> jadwalShalat = Application.alarmDatabase.getJadwalShalat(Utils.currentDateInMilis());
        boolean getNext = false;
        for (Shalat s : jadwalShalat) {
            if(s.getTimestamp() == Utils.currentDateInMilis() || getNext) {

                if(System.currentTimeMillis() <= s.getMidnightTime()  || getNext){

                    Calendar currentCal = Calendar.getInstance();
                    currentCal.setTimeInMillis(s.getMidnightTime());

                    waktuSelanjutnya.setText(Utils.dateToDefaultFormat(currentCal));

                }else{
                    getNext = true;
                    continue;
                }
                break;
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.OkButton) {

            for (int i = 0; i < res.length; i++) {
                Const.malamHarian(days[i], ((CircleCheckBox) ButterKnife.findById(this, res[i])).isChecked());
            }

            Const.ringtone(Utils.ALARM_TYPE.SHOLAT_MALAM, spinnerLail.getSelectedItemPosition());
            if (Const.alarmMalamAktif()) {
                Utils.resetAlarmSholat();
            }
            finish();
        }
        if (id == R.id.CancelButton) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }


//
//    public void setRingtone() {
//        //Set Ringtone
//        Spinner spinner = (Spinner) findViewById(R.id.spinner_lail);
//
//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
//                R.array.audio_array, android.R.layout.simple_spinner_item);
//
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//
//        spinner.setAdapter(adapter);
//
//        spinner.setOnItemSelectedListener(this);
//    }
//
//
//    public  void dismissAlarm(){
//        alarm_manager.cancel(pending_intent3);
//        intent_receiver_lail.putExtra("extra", "alarm off");
//        intent_receiver_lail.putExtra("sound_choice", choose_sound);
//        sendBroadcast(intent_receiver_lail);
//    }
//
//    public void dialogNotif(){
//        //Pop Up Notif
//        if(getIntent().getStringExtra("alarm")!=null)
//        {
//            String strdata = getIntent().getExtras().getString("alarm");
//            strdata.equals("matikan");
//
//            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SetSholatLail.this);
//            dialogBuilder.setTitle("Sudah Sholat Malam?")
//                    .setMessage("“Dan sebutlah nama Rabb-mu pada (waktu) pagi dan petang. " +
//                            "Dan pada sebagian dari malam, maka sujudlah kepada-Nya dan bertasbihlah kepada-Nya " +
//                            "pada bagian yang panjang di malam hari.“ (TQS.Al-Insaan 25-26)\n")
//                    .setPositiveButton("Sudah", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            Toast.makeText(SetSholatLail.this, "Barokallah", Toast.LENGTH_SHORT).show();
//                            dismissAlarm();
//
//                        }
//                    })
//                    .setNegativeButton("Belum", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            Toast.makeText(SetSholatLail.this, "Ayo disegerakan..", Toast.LENGTH_SHORT).show();
//                            dismissAlarm();
//                        }
//                    })
//                    .setNeutralButton("Nanti", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            Toast.makeText(SetSholatLail.this, "Ayo disegerakan..", Toast.LENGTH_SHORT).show();
//                            dismissAlarm();
//                                /*AlertDialog.Builder dialogNanti = new AlertDialog.Builder(MainActivity.this);
//                                dialogNanti.setItems();*/
//                        }
//                    })
//                    .show();
//        }
//
//
//    }
//
//    public static Context getAppContext() {
//        return staticContext;
//    }
}