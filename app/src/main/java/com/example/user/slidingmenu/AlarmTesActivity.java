package com.example.user.slidingmenu;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.slidingmenu.model.Alarm;
import com.example.user.slidingmenu.model.AlarmDatabase;

import java.util.Random;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class AlarmTesActivity extends Activity {

    @InjectView(R.id.text)
    TextView text;
    @InjectView(R.id.hadist)
    TextView hadist;
    @InjectView(R.id.sudah)
    Button sudah;
    @InjectView(R.id.belum)
    Button belum;
    @InjectView(R.id.nanti)
    Button nanti;

    private long id;
    private Utils.ALARM_TYPE type;
    private String msg;

    private NotificationManager notificationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_alarm_tes);
        ButterKnife.inject(this);

        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        if (getIntent().getExtras() != null) {
            id = getIntent().getExtras().getLong("id");
            msg = getIntent().getExtras().getString("msg");
            type = Utils.ALARM_TYPE.values()[getIntent().getExtras().getInt("type")];
            hadist.setText(Utils.hadistByType(type));
            switch (type) {
                case PUASA_KAMIS:
                    text.setText("Jangan lupa Puasa Senin Kamis");
                    break;
                case PUASA_SENIN:
                    text.setText("Jangan lupa Puasa Senin Kamis");
                    break;
                case PUASA_DAUD:
                    text.setText("Jangan lupa Puasa Daud");
                    break;
                case PUASA_TENGAH_BULAN:
                    text.setText("Jangan lupa Puasa Tengah Bulan");
                    break;
                case SHOLAT_DHUHA:
                    text.setText("Sudah Sholat Dhuha?");
                    break;
                case SHOLAT_MALAM:
                    text.setText("Sudah Sholat Malam?");
                    break;
                case SHOLAT_WAJIB_ASHAR:
                    text.setText("Sudah Sholat Ashar?");
                    break;
                case SHOLAT_WAJIB_DHUHUR:
                    text.setText("Sudah Sholat Dhuhur?");
                    break;
                case SHOLAT_WAJIB_ISYA:
                    text.setText("Sudah Sholat Isya?");
                    break;
                case SHOLAT_WAJIB_MAGHRIB:
                    text.setText("Sudah Sholat Maghrib?");
                    break;
                case SHOLAT_WAJIB_SUBUH:
                    text.setText("Sudah Sholat Subuh?");
                    break;
            }
        }

    }

    @OnClick({R.id.sudah, R.id.belum, R.id.nanti})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sudah:
                finish();
                stopService(new Intent(this, AlarmPlayingService.class));
                Toast.makeText(this, "Barokallah", Toast.LENGTH_SHORT).show();
                notificationManager.cancel((int)id);
                Application.alarmDatabase.alarmDone(id, AlarmDatabase.ALARM_STATUS.SUDAH);
                break;
            case R.id.belum:
                finish();
                stopService(new Intent(this, AlarmPlayingService.class));
                Toast.makeText(this, "Ayo disegerakan..", Toast.LENGTH_SHORT).show();
                notificationManager.cancel((int)id);
                Application.alarmDatabase.alarmDone(id, AlarmDatabase.ALARM_STATUS.ABAIKAN);
                break;
            case R.id.nanti:
                finish();
                stopService(new Intent(this, AlarmPlayingService.class));
                Toast.makeText(this, "Ayo disegerakan..", Toast.LENGTH_SHORT).show();
                Utils.setAlarm(new Alarm(new Random().nextInt(), System.currentTimeMillis() + (1000 * 60 * 5), type, msg));
                notificationManager.cancel((int)id);
                Application.alarmDatabase.alarmDone(id, AlarmDatabase.ALARM_STATUS.DELAY);
                break;
        }

        if(type.ordinal()>= Utils.ALARM_TYPE.PUASA_SENIN.ordinal()){
            Utils.resetAlarmPuasa();
        }else{
            Utils.resetAlarmSholat();
        }
    }
}