package com.example.user.slidingmenu;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

public class TilawahActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    LinearLayout setWaktuTilawah;
    AlarmManager alarm_manager;
    TextView update_text_tilawah;
    Button button;
    Context context;
    static Context staticContext;
    PendingIntent pending_intent_tilawah;
    private TimePickerDialog timePickerDialog;
    int choose_sound;
    Intent intent_receiver_tilawah;


    private int mHour, mMinute;
    private Toolbar TopToolbar;
    Calendar time;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tilawah);
        this.context = this;
        staticContext = this.getApplicationContext();
        TopToolbar = (Toolbar) findViewById(R.id.toolbar); // Attaching the layout to the toolbar object
        setSupportActionBar(TopToolbar);

/*        getActionBar().setDisplayHomeAsUpEnabled(true);*/

        setTime();
        setRingtone();
        dialogNotif();
    }
    public void alarmManager() {
        alarm_manager = (AlarmManager) getSystemService(ALARM_SERVICE);

    }

    public void setRingtone() {
        //Set Ringtone
        Spinner spinner = (Spinner) findViewById(R.id.spinner_tilawah);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.audio_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(this);
    }

    public void onClick(View v){

        Drawable dr = getResources().getDrawable(R.drawable.rounded_bg);
        dr.setColorFilter(Color.parseColor("#FFCDD2"), PorterDuff.Mode.SRC_ATOP);

        switch (v.getId()) {
            case R.id.btn_tilawah_ahad:
                if (button == null) {
                    button = (Button) findViewById(v.getId());
                } else {
                    button.setBackgroundResource(R.drawable.rounded_bg);
                    button = (Button) findViewById(v.getId());
                    time.set(Calendar.DAY_OF_YEAR, Calendar.SUNDAY);
                }
                button.setBackgroundDrawable(dr);

                break;

            case R.id.btn_tilawah_senin:
                if (button == null) {
                    button = (Button) findViewById(v.getId());
                } else {
                    button.setBackgroundResource(R.drawable.rounded_bg);
                    button = (Button) findViewById(v.getId());
                    time.set(Calendar.DAY_OF_YEAR, Calendar.MONDAY);
                }
                button.setBackgroundDrawable(dr);

                break;

            case R.id.btn_tilawah_selasa:
                if (button == null) {
                    button = (Button) findViewById(v.getId());
                } else {
                    button.setBackgroundResource(R.drawable.rounded_bg);
                    button = (Button) findViewById(v.getId());
                    time.set(Calendar.DAY_OF_YEAR, Calendar.TUESDAY);
                }
                button.setBackgroundDrawable(dr);

                break;

            case R.id.btn_tilawah_rabu:
                if (button == null) {
                    button = (Button) findViewById(v.getId());
                } else {
                    button.setBackgroundResource(R.drawable.rounded_bg);
                    button = (Button) findViewById(v.getId());
                    time.set(Calendar.DAY_OF_YEAR, Calendar.WEDNESDAY);
                }
                button.setBackgroundDrawable(dr);

                break;

            case R.id.btn_tilawah_kamis:
                if (button == null) {
                    button = (Button) findViewById(v.getId());
                } else {
                    button.setBackgroundResource(R.drawable.rounded_bg);
                    button = (Button) findViewById(v.getId());
                    time.set(Calendar.DAY_OF_YEAR, Calendar.THURSDAY);
                }
                button.setBackgroundDrawable(dr);

                break;

            case R.id.btn_tilawah_jumat:
                if (button == null) {
                    button = (Button) findViewById(v.getId());
                } else {
                    button.setBackgroundResource(R.drawable.rounded_bg);
                    button = (Button) findViewById(v.getId());
                    time.set(Calendar.DAY_OF_YEAR, Calendar.FRIDAY);
                }
                button.setBackgroundDrawable(dr);

                break;

            case R.id.btn_tilawah_sabtu:
                if (button == null) {
                    button = (Button) findViewById(v.getId());
                } else {
                    button.setBackgroundResource(R.drawable.rounded_bg);
                    button = (Button) findViewById(v.getId());
                    time.set(Calendar.DAY_OF_YEAR, Calendar.SATURDAY);
                }
                button.setBackgroundDrawable(dr);

                break;


            default:
                break;
        }


        /*DatePickerDialog datePickerDialog = new DatePickerDialog(SetSholatDhuha.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                time.set(Calendar.DAY_OF_YEAR, Calendar.SATURDAY);

            }
        }, time.get(Calendar.YEAR), time.get(Calendar.MONTH), time.get(Calendar.DAY_OF_YEAR));*/
    }

    public void setTime() {
        //Set Time
        alarmManager();
        setRingtone();

        setWaktuTilawah = (LinearLayout) findViewById(R.id.linier_SWtilawah);
        update_text_tilawah = (TextView) findViewById(R.id.update_text_tilawah);

        time = Calendar.getInstance();
        final Intent intent_receiver = new Intent(this, Alarm_Receiver.class);
        intent_receiver_tilawah = intent_receiver;


        setWaktuTilawah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //ALARM ON
                timePickerDialog = new TimePickerDialog(TilawahActivity.this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                time.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                time.set(Calendar.MINUTE, minute);

                                mHour = time.get(Calendar.HOUR_OF_DAY);
                                mMinute = time.get(Calendar.MINUTE);

                                String hour_string = String.valueOf(mHour);
                                String minute_string = String.valueOf(mMinute);

                                if (mMinute < 10) {
                                    minute_string = "0" + String.valueOf(mMinute);
                                }

                                update_text_tilawah.setText(hour_string + ":" + minute_string);
                                Log.e("jam ", String.valueOf(mHour));
                                Log.e("menit ", String.valueOf(mMinute));


                            }
                        }, mHour, mMinute, true);
                timePickerDialog.show(); // sampe sini kan ngeluarin jam nya


            }

        });


    }

    public void dismissAlarm() {
        alarm_manager.cancel(pending_intent_tilawah);
        intent_receiver_tilawah.putExtra("extra", "alarm off");
        intent_receiver_tilawah.putExtra("sound_choice", choose_sound);
        sendBroadcast(intent_receiver_tilawah);
    }

    public void dialogNotif() {
        //Pop Up Notif
        if (getIntent().getStringExtra("alarm") != null) {
            String strdata = getIntent().getExtras().getString("alarm");
            strdata.equals("matikan");

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TilawahActivity.this);
            dialogBuilder.setTitle("Sudah Tilawah?")
                    .setMessage("Sebaik-baik manusia adalah yang bermanfaat untuk orang lain. (HR. Ahmad)")
                    .setPositiveButton("Sudah", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Toast.makeText(TilawahActivity.this, "Barokallah", Toast.LENGTH_SHORT).show();
                            dismissAlarm();

                        }
                    })
                    .setNegativeButton("Belum", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Toast.makeText(TilawahActivity.this, "Ayo disegerakan..", Toast.LENGTH_SHORT).show();
                            dismissAlarm();
                        }
                    })
                    .setNeutralButton("Nanti", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Toast.makeText(TilawahActivity.this, "Ayo disegerakan..", Toast.LENGTH_SHORT).show();
                            dismissAlarm();
                                /*AlertDialog.Builder dialogNanti = new AlertDialog.Builder(MainActivity.this);
                                dialogNanti.setItems();*/
                        }
                    })
                    .show();
        }


    }

    public static Context getAppContext() {
        return staticContext;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        /*if (id == R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }*/
        //noinspection SimplifiableIfStatement

        //ALARM ON
        if (id == R.id.OkButton) {
            //setTime();
            Toast.makeText(TilawahActivity.this, "simpan", Toast.LENGTH_LONG).show();

            //final Calendar c = Calendar.getInstance();
            final Intent intent_receiver = new Intent(this, Alarm_Receiver.class);
            intent_receiver_tilawah = intent_receiver;

            Log.e("jam ", String.valueOf(mHour));
            Log.e("menit ", String.valueOf(mMinute));

            //kirim data ke receiver
            intent_receiver.putExtra("extra", "alarm on");
            //Memilih Lagu
            intent_receiver.putExtra("sound_choice", choose_sound);
            Log.e("the sound id is ", String.valueOf(choose_sound));

            pending_intent_tilawah = PendingIntent.getBroadcast(TilawahActivity.this, 3, intent_receiver, PendingIntent.FLAG_UPDATE_CURRENT);
            Log.i("asdadasdasd", mHour + " " + mMinute + " " + time.getTimeInMillis());
            Log.v("TIMESET", "" + time.getTimeInMillis());
            alarm_manager.set(AlarmManager.RTC_WAKEUP, time.getTimeInMillis(), pending_intent_tilawah);

        }
        if (id == R.id.CancelButton) {
            Toast.makeText(TilawahActivity.this, "batalkan", Toast.LENGTH_LONG).show();/*
            switch_sholat_wajib = (Switch) findViewById(R.id.switch_sholat_wajib);
            switch_sholat_wajib.isChecked();*/

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        choose_sound = (int) id;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
