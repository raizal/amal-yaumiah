package com.example.user.slidingmenu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import java.util.Calendar;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class JenisPuasaActivity extends AppCompatActivity {

    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jenis_puasa);
        ButterKnife.inject(this);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnCheckedChanged({R.id.switch_puasa_daud,R.id.switch_puasa_tengah_bulan,R.id.switch_puasa_senin})
    public void onCheckedChanged(CompoundButton view,boolean value){
        switch (view.getId()){
            case R.id.switch_puasa_daud:
                Const.alarmPuasadaudAktif(view.isChecked());
                Const.awalDaud(Utils.currentDateInMilis());
                break;

            case R.id.switch_puasa_senin:
                Const.alarmPuasaSeninAktif(view.isChecked());
                break;

            case R.id.switch_puasa_tengah_bulan:
                Const.alarmPuasatengahBulanAktif(view.isChecked());
                break;
        }
        Utils.resetAlarmPuasa();
    }

    @OnClick({R.id.BtnPuasaSenin, R.id.BtnPuasaTengahBulan, R.id.BtnPuasaDaud})
    public void onClick(View view) {
        Intent intent = new Intent(this,PuasaActivity.class);
        switch (view.getId()) {
            case R.id.BtnPuasaSenin:
                intent.putExtra("type", Utils.ALARM_TYPE.PUASA_SENIN.ordinal());
                break;
            case R.id.BtnPuasaTengahBulan:
                intent.putExtra("type", Utils.ALARM_TYPE.PUASA_TENGAH_BULAN.ordinal());
                break;
            case R.id.BtnPuasaDaud:
                intent.putExtra("type", Utils.ALARM_TYPE.PUASA_DAUD.ordinal());
                break;
        }
        startActivity(intent);
    }
}
