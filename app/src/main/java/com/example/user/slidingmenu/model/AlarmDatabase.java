package com.example.user.slidingmenu.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.user.slidingmenu.Utils;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by raizal.pregnanta on 10/10/2016.
 */
public class AlarmDatabase extends SQLiteAssetHelper {

    private static final String DATABASE_NAME = "jadwal.db";
    private static final int DATABASE_VERSION = 1;

    public AlarmDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public boolean insertShalat(Shalat time) {
        if (isExists("shalat", "date", time.getTimestamp() + ""))
            return false;

        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("date", time.getTimestamp());
        contentValues.put("fajr", time.getFajrString());
        contentValues.put("sunrise", time.getSunriseString());
        contentValues.put("dhuhr", time.getDhuhrString());
        contentValues.put("asr", time.getAsrString());
        contentValues.put("maghrib", time.getMaghribString());
        contentValues.put("isha", time.getIshaString());
        contentValues.put("imsak", time.getImsakString());
        contentValues.put("midnight", time.getMidnightString());
        try {
            db.insert("shalat", null, contentValues);
        } catch (Exception e) {
            return false;
        } finally {
            db.close();
        }
        return true;
    }


    public boolean insertAlarm(Alarm alarm) {
        if (isExists("alarm", "id", alarm.getId() + ""))
            return false;

        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", alarm.getId());
        contentValues.put("time", alarm.getTime());
        contentValues.put("text", alarm.getText());
        contentValues.put("status", alarm.getStatus());
        contentValues.put("type", alarm.getType());
        try {
            db.insert("alarm", null, contentValues);
        } catch (Exception e) {
            return false;
        } finally {

        }
        return true;
    }

    public boolean insertPuasa(Puasa puasa) {
        if (isExists("puasa", "date", puasa.getDate() + ""))
            return false;

        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("id", puasa.getId());
        contentValues.put("time", puasa.getTime());
        contentValues.put("name", puasa.getName());
        contentValues.put("date", puasa.getDate());
        contentValues.put("type", puasa.getType());
        try {
            db.insert("puasa", null, contentValues);
        } catch (Exception e) {
            return false;
        } finally {
            db.close();
        }
        return true;
    }

    public void alarmDone(long id, ALARM_STATUS status) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("update alarm set status=" + status.ordinal() + ",action_time=" + System.currentTimeMillis() + " where id=" + id);
    }

    public List<Alarm> getAlarm(long start, long end) {
        List<Alarm> data = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;

        try {
            c = db.rawQuery("select * from alarm where  status=0 and time>=" + start + " and time<=" + end, null);

            if (c.moveToFirst()) {
                do {
                    Alarm alarm = new Alarm(
                            c.getLong(c.getColumnIndex("id")),
                            c.getLong(c.getColumnIndex("time")),
                            c.getString(c.getColumnIndex("text")),
                            c.getInt(c.getColumnIndex("status")),
                            c.getInt(c.getColumnIndex("type")),
                            c.getLong(c.getColumnIndex("action_time"))
                    );
                    data.add(alarm);
                } while (c.moveToNext());
            }

        } finally {
            db.close();
            if (c != null)
                c.close();
        }

        return data;
    }

    public List<Alarm> getAlarmSholat() {
        return getAlarm("type<=" + Utils.ALARM_TYPE.SHOLAT_MALAM.ordinal());
    }

    public List<Alarm> getAlarmPuasa() {
        return getAlarm("type>" + Utils.ALARM_TYPE.SHOLAT_MALAM.ordinal());
    }

    public List<Alarm> getDoneAlarm(String additionalCase) {
        List<Alarm> data = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;

        try {
            c = db.rawQuery("select * from alarm where status>0 and " + additionalCase, null);

            if (c.moveToFirst()) {
                do {
                    Alarm alarm = new Alarm(
                            c.getLong(c.getColumnIndex("id")),
                            c.getLong(c.getColumnIndex("time")),
                            c.getString(c.getColumnIndex("text")),
                            c.getInt(c.getColumnIndex("status")),
                            c.getInt(c.getColumnIndex("type")),
                            c.getLong(c.getColumnIndex("action_time"))
                    );
                    data.add(alarm);
                } while (c.moveToNext());
            }

        } finally {
            db.close();
            if (c != null)
                c.close();
        }

        return data;
    }

    public List<Alarm> getAlarm(String additionalCase) {
        List<Alarm> data = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;

        try {
            c = db.rawQuery("select * from alarm where status=0 and " + additionalCase, null);

            if (c.moveToFirst()) {
                do {
                    Alarm alarm = new Alarm(
                            c.getLong(c.getColumnIndex("id")),
                            c.getLong(c.getColumnIndex("time")),
                            c.getString(c.getColumnIndex("text")),
                            c.getInt(c.getColumnIndex("status")),
                            c.getInt(c.getColumnIndex("type")),
                            c.getLong(c.getColumnIndex("action_time"))
                    );
                    data.add(alarm);
                } while (c.moveToNext());
            }

        } finally {
            db.close();
            if (c != null)
                c.close();
        }

        return data;
    }

    public List<Shalat> getJadwalShalat(long start) {
        List<Shalat> data = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;

        try {
            c = db.rawQuery("select * from shalat where date>=" + start, null);

            if (c.moveToFirst()) {
                do {
                    Shalat alarm = new Shalat(
                            c.getLong(c.getColumnIndex("date")),
                            c.getString(c.getColumnIndex("fajr")),
                            c.getString(c.getColumnIndex("sunrise")),
                            c.getString(c.getColumnIndex("dhuhr")),
                            c.getString(c.getColumnIndex("asr")),
                            c.getString(c.getColumnIndex("maghrib")),
                            c.getString(c.getColumnIndex("isha")),
                            c.getString(c.getColumnIndex("imsak")),
                            c.getString(c.getColumnIndex("midnight"))
                    );
                    data.add(alarm);
                } while (c.moveToNext());
            }

        } finally {
            db.close();
            if (c != null)
                c.close();
        }

        return data;
    }

    public List<Shalat> getJadwalShalat(long start, long end) {
        List<Shalat> data = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();
        Cursor c = null;

        try {
            c = db.rawQuery("select * from shalat where date>=" + start + " and date<=" + end, null);

            if (c.moveToFirst()) {
                do {
                    Shalat alarm = new Shalat(
                            c.getLong(c.getColumnIndex("date")),
                            c.getString(c.getColumnIndex("fajr")),
                            c.getString(c.getColumnIndex("sunrise")),
                            c.getString(c.getColumnIndex("dhuhr")),
                            c.getString(c.getColumnIndex("asr")),
                            c.getString(c.getColumnIndex("maghrib")),
                            c.getString(c.getColumnIndex("isha")),
                            c.getString(c.getColumnIndex("imsak")),
                            c.getString(c.getColumnIndex("midnight"))
                    );
                    data.add(alarm);
                } while (c.moveToNext());
            }

        } finally {
            db.close();
            if (c != null)
                c.close();
        }

        return data;
    }

    public boolean isExists(String table, String column, String value) {
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + table + " where " + column + "=\"" + value + "\"", null);
        boolean result = cursor.moveToFirst();
        cursor.close();
        db.close();
        return result;
    }

    public enum ALARM_STATUS {BELUM, SUDAH, DELAY, ABAIKAN}
}