package com.example.user.slidingmenu;

import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.user.slidingmenu.model.DailyTime;
import com.example.user.slidingmenu.model.Shalat;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by raizal.pregnanta on 21/09/2016.
 */
public class Const {

    private static int downloadedSchedule = 0;
    private static int errorTry = 0;

    public static void downloadAllYear(final double lat, final double lng, final DownloadJadwalListener downloadJadwalListener) {
        downloadAllYear(Calendar.getInstance(),lat,lng,downloadJadwalListener);
    }

    public static void downloadAllYear(final Calendar cal,final double lat, final double lng, final DownloadJadwalListener downloadJadwalListener) {
        downloadedSchedule = 0;
        downloadJadwal(lat, lng, cal.get(Calendar.MONTH) + 1, cal.get(Calendar.YEAR), new DownloadJadwalListener() {
            @Override
            public void onComplete() {
                synchronized (Const.class) {
                    errorTry = 0;
                    cal.add(Calendar.MONTH, 1);
                    downloadedSchedule++;
                    if (downloadedSchedule < 12) {
                        downloadJadwal(lat, lng, cal.get(Calendar.MONTH) + 1, cal.get(Calendar.YEAR), this);
                    } else {
                        if (downloadJadwalListener != null) {
                            downloadJadwalListener.onComplete();
                        }
                    }
                }
            }

            @Override
            public void onError(ANError anError) {
                errorTry++;
                if(errorTry>=12){
                    if (downloadJadwalListener != null) {
                        downloadJadwalListener.onError(anError);
                    }
                }else {
                    downloadJadwal(lat, lng, cal.get(Calendar.MONTH) + 1, cal.get(Calendar.YEAR), this);
                }
            }
        });
    }

    public static void downloadJadwal(double lat, double lng, int bulan, int tahun, final DownloadJadwalListener downloadJadwalListener) {
        String url = "http://api.aladhan.com/calendar?latitude=" + lat + "&longitude=" + lng + "&timezonestring=Asia%2FJakarta&method=3&month=" + bulan + "&year=" + tahun;

        AndroidNetworking.get(url)
                .setTag("request jadwal")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getInt("code") == 200) {

                                String responseText = response.getString("data");

                                List<DailyTime> data = new Gson().fromJson(responseText, new TypeToken<List<DailyTime>>() {
                                }.getType());
                                if (data != null) {
                                    for (DailyTime dailyTime : data) {
                                        dailyTime.timings.setTimestamp(Utils.extractDateOnly(dailyTime.date.getTimestamp()));
                                        Application.alarmDatabase.insertShalat(dailyTime.timings);
                                    }

                                    if (downloadJadwalListener != null)
                                        downloadJadwalListener.onComplete();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        if (downloadJadwalListener != null)
                            downloadJadwalListener.onError(anError);
                    }
                });
    }

    public static void alarmWajibAktif(boolean aktif) {
        Application.sharedPreferences.edit().putBoolean("wajib", aktif).commit();
    }

    public static boolean alarmWajibAktif() {
        return Application.sharedPreferences.getBoolean("wajib", false);
    }

    public static boolean wajibHarian(WAJIB_TYPE type) {
        return Application.sharedPreferences.getBoolean("wajib" + type.ordinal(), true);
    }

    ;

    public static void wajibHarian(WAJIB_TYPE type, boolean aktif) {
        Application.sharedPreferences.edit().putBoolean("wajib" + type.ordinal(), aktif).commit();
    }

    public static void alarmDhuhaAktif(boolean aktif) {
        Application.sharedPreferences.edit().putBoolean("dhuha", aktif).commit();
    }

    public static boolean alarmDhuhaAktif() {
        return Application.sharedPreferences.getBoolean("dhuha", false);
    }

    public static void alarmPuasaSeninAktif(boolean aktif) {
        Application.sharedPreferences.edit().putBoolean("puasa_senin", aktif).commit();
    }

    public static boolean alarmPuasaSeninAktif() {
        return Application.sharedPreferences.getBoolean("puasa_senin", false);
    }

    public static void alarmPuasakamisAktif(boolean aktif) {
        Application.sharedPreferences.edit().putBoolean("puasa_kamis", aktif).commit();
    }

    public static boolean alarmPuasakamisAktif() {
        return Application.sharedPreferences.getBoolean("puasa_kamis", false);
    }

    public static void alarmPuasatengahBulanAktif(boolean aktif) {
        Application.sharedPreferences.edit().putBoolean("puasa_tengahBulan", aktif).commit();
    }

    public static boolean alarmPuasatengahBulanAktif() {
        return Application.sharedPreferences.getBoolean("puasa_tengahBulan", false);
    }

    public static void alarmPuasadaudAktif(boolean aktif) {
        Application.sharedPreferences.edit().putBoolean("puasa_daud", aktif).commit();
    }

    public static boolean alarmPuasadaudAktif() {
        return Application.sharedPreferences.getBoolean("puasa_daud", false);
    }

    public static boolean alarmMalamAktif() {
        return Application.sharedPreferences.getBoolean("malam", false);
    }

    public static long waktuAlarm(Utils.ALARM_TYPE type){
        return Application.sharedPreferences.getLong("alarm_waktu_"+type.ordinal(),0);
    }

    public static void waktuAlarm(Utils.ALARM_TYPE type,long time){
        Application.sharedPreferences.edit().putLong("alarm_waktu_"+type.ordinal(),time).commit();
    }

    public static boolean dhuhaHarian(int hari) {
        return Application.sharedPreferences.getBoolean("dhuha" + hari, false);
    }

    public static void dhuhaHarian(int hari, boolean aktif) {
        Application.sharedPreferences.edit().putBoolean("dhuha" + hari, aktif).commit();
    }

    public static void alarmMalamAktif(boolean aktif) {
        Application.sharedPreferences.edit().putBoolean("malam", aktif).commit();
    }

    public static boolean malamHarian(int hari) {
        return Application.sharedPreferences.getBoolean("malam" + hari, false);
    }

    public static void malamHarian(int hari, boolean aktif) {
        Application.sharedPreferences.edit().putBoolean("malam" + hari, aktif).commit();
    }

    public static void awalDaud(long date){
        Application.sharedPreferences.edit().putLong("awal_daud",date).commit();
    }

    public static long awalDaud(){
        return Application.sharedPreferences.getLong("awal_daud",0);
    }

    public static boolean daudHarian(int hari) {
        return Application.sharedPreferences.getBoolean("daud" + hari, false);
    }

    public static void daudHarian(int hari, boolean aktif) {
        Application.sharedPreferences.edit().putBoolean("daud" + hari, aktif).commit();
    }

    public static double lat() {
        return Double.valueOf(Application.sharedPreferences.getString("lat", "0"));
    }

    public static void lat(double lat) {
        Application.sharedPreferences.edit().putString("lat", lat + "").commit();
    }

    public static double lng() {
        return Double.valueOf(Application.sharedPreferences.getString("lng", "0"));
    }

    public static void lng(double lng) {
        Application.sharedPreferences.edit().putString("lng", lng + "").commit();
    }

    public static String formateDate(long timestamp) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp);
        return String.format("%02d", cal.get(Calendar.DAY_OF_MONTH)) + " " + (new SimpleDateFormat("MMM").format(cal.getTime())) + " " + cal.get(Calendar.YEAR);
    }

    public static boolean isDataAvailable() {
        return Application.alarmDatabase.getJadwalShalat(Utils.currentDateInMilis()).size()>0;
    }

    public static int idFromTimeInMilis(Long milis) {
        return Integer.valueOf(String.valueOf(milis).substring(6, (milis + "").length() - 1));
    }

    public static void ringtone(Utils.ALARM_TYPE type, int index) {
        Application.sharedPreferences.edit().putInt("ringtone_" + type.ordinal(), index).commit();
    }

    public static int ringtone(Utils.ALARM_TYPE type) {
        return Application.sharedPreferences.getInt("ringtone_" + type.ordinal(), 0);
    }

    public enum WAJIB_TYPE {SUBUH, DHUHUR, ASHAR, MAGHRIB, ISYA}

    public interface DownloadJadwalListener {
        public void onComplete();

        public void onError(ANError anError);
    }

}