package com.example.user.slidingmenu;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import java.security.Provider;
import java.util.Random;

/**
 * Created by user on 08/06/2016.
 */
public class AlarmPlayingService extends Service {

    MediaPlayer media_song;
    final static String GROUP_KEY_EMAILS = "group_key_emails";


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("LocalService", "Received start id " + startId + ": " + intent);

        Integer alarm_sound_choice ;
        Utils.ALARM_TYPE type = Utils.ALARM_TYPE.values()[intent.getExtras().getInt("type")];
        alarm_sound_choice = Const.ringtone(type);

        //if no music, user press "alarm on"
        //music should start playing
        if (media_song==null || !media_song.isPlaying()) {
            Log.e("There is no music, ", "and you want to start");


            //Play the sound depending on the passed choice id
            switch (alarm_sound_choice) {
                case 0:
                    media_song = MediaPlayer.create(this, R.raw.adzan);
                    media_song.start();
                    Log.e("Playing sound ", alarm_sound_choice.toString());
                    break;
                case 1:
                    media_song = MediaPlayer.create(this, R.raw.ibu);
                    media_song.start();
                    Log.e("Playing sound ", alarm_sound_choice.toString());
                    break;
                case 2:
                    media_song = MediaPlayer.create(this, R.raw.ijinkan);
                    media_song.start();
                    Log.e("Playing sound ", alarm_sound_choice.toString());
                    break;
                case 3:
                    media_song = MediaPlayer.create(this, R.raw.rimba);
                    media_song.start();
                    Log.e("Playing sound ", alarm_sound_choice.toString());
                    break;
                case 4:
                    media_song = MediaPlayer.create(this, R.raw.sarjana);
                    media_song.start();
                    Log.e("Playing sound ", alarm_sound_choice.toString());
                    break;
                default:
                    media_song = MediaPlayer.create(this, R.raw.adzan);
                    media_song.start();
                    Log.e("Playing sound ", alarm_sound_choice.toString());
                    break;
            }

        }
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        try {
            if(media_song!=null) {
                media_song.stop();
                media_song.reset();
            }
        }catch (Exception e){

        }
        super.onDestroy();
    }


}
