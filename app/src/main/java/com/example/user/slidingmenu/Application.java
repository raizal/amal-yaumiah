package com.example.user.slidingmenu;

import android.content.SharedPreferences;
import android.location.Location;

import com.androidnetworking.AndroidNetworking;
import com.example.user.slidingmenu.model.AlarmDatabase;
import com.google.android.gms.location.LocationRequest;

import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Subscription;
import rx.functions.Action1;

/**
 * Created by raizal.pregnanta on 21/09/2016.
 */
public class Application extends android.app.Application {

    public static SharedPreferences sharedPreferences;
    public static Application instance;
    public static AlarmDatabase alarmDatabase;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        AndroidNetworking.initialize(getApplicationContext());
        sharedPreferences = getSharedPreferences("data", MODE_PRIVATE);
        alarmDatabase = new AlarmDatabase(this);
    }
}

