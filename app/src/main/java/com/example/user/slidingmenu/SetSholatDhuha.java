package com.example.user.slidingmenu;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.user.slidingmenu.model.Shalat;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class SetSholatDhuha extends AppCompatActivity {

    int res[] = new int[]{R.id.btn_dhuha_ahad, R.id.btn_dhuha_senin, R.id.btn_dhuha_selasa, R.id.btn_dhuha_rabu, R.id.btn_dhuha_kamis, R.id.btn_dhuha_jumat, R.id.btn_dhuha_sabtu};
    int days[] = new int[]{Calendar.SUNDAY, Calendar.MONDAY, Calendar.TUESDAY, Calendar.WEDNESDAY, Calendar.THURSDAY, Calendar.FRIDAY, Calendar.SATURDAY};
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.spinner_dhuha)
    Spinner spinnerDhuha;
    @InjectView(R.id.update_text_dhuha)
    TextView waktuSelanjutnya;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_sholat_dhuha);
        ButterKnife.inject(this);
        setSupportActionBar(toolbar);
        for (int i = 0; i < res.length; i++) {
            ((CircleCheckBox) ButterKnife.findById(this, res[i])).setChecked(Const.dhuhaHarian(days[i]));
        }
        
        spinnerDhuha.setSelection(Const.ringtone(Utils.ALARM_TYPE.SHOLAT_DHUHA));

        List<Shalat> jadwalShalat = Application.alarmDatabase.getJadwalShalat(Utils.currentDateInMilis());
        boolean getNext = false;
        for (Shalat s : jadwalShalat) {
            if ( s.getTimestamp() == Utils.currentDateInMilis() || getNext) {

                if (System.currentTimeMillis() <= s.getSunriseTime() || getNext) {

                    Calendar currentCal = Calendar.getInstance();
                    currentCal.setTimeInMillis(s.getSunriseTime());
                    currentCal.add(Calendar.MINUTE,15);
                    String text = Utils.dateToDefaultFormat(currentCal);
                    waktuSelanjutnya.setText(text);

                } else {
                    getNext = true;
                    continue;
                }
                break;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.OkButton) {
            for (int i = 0; i < res.length; i++) {
                Const.dhuhaHarian(days[i], ((CircleCheckBox) ButterKnife.findById(this, res[i])).isChecked());
            }
            Const.ringtone(Utils.ALARM_TYPE.SHOLAT_DHUHA, spinnerDhuha.getSelectedItemPosition());
            if (Const.alarmDhuhaAktif()) {
                Utils.resetAlarmSholat();
            }
            finish();
        }
        if (id == R.id.CancelButton) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

}
