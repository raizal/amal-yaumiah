package com.example.user.slidingmenu.model;

/**
 * Created by raizal.pregnanta on 06/10/2016.
 */
public class Date {
    public String readable;
    public String timestamp;

    public long getTimestamp() {
        return Long.valueOf(timestamp)*1000;
    }
}
