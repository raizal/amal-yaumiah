package com.example.user.slidingmenu;

import android.annotation.TargetApi;
import android.app.TimePickerDialog;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.github.msarhan.ummalqura.calendar.UmmalquraCalendar;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

public class PuasaActivity extends AppCompatActivity implements CompactCalendarView.CompactCalendarViewListener {

    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.calendar)
    CompactCalendarView calendar;
    @InjectView(R.id.caption_calendar)
    TextView captionCalendar;
    @InjectView(R.id.alarm_time)
    TextView alarmTime;
    @InjectView(R.id.spinner_puasa)
    Spinner spinnerPuasa;

    private Utils.ALARM_TYPE type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_puasa);
        ButterKnife.inject(this);

        calendar.setListener(this);

        setSupportActionBar(toolbar);
        if (getIntent().getExtras() != null) {
            type = Utils.ALARM_TYPE.values()[getIntent().getExtras().getInt("type")];

            spinnerPuasa.setSelection(Const.ringtone(type));

            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(Const.waktuAlarm(type));

            alarmTime.setText( String.format("%02d", cal.get(Calendar.HOUR_OF_DAY))
                     + ":" + String.format("%02d", cal.get(Calendar.MINUTE)));

            switch (type) {
                case PUASA_DAUD:
                    setTitle("Puasa Daud");
                    break;
                case PUASA_TENGAH_BULAN:
                    setTitle("Puasa Ayyamul Bidh");
                    break;
                case PUASA_SENIN:
                    setTitle("Puasa Senin Kamis");
                    break;
            }

            refresh(Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.YEAR));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        /*if (id == R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }*/

        //ALARM ON
        if (id == R.id.OkButton) {

            int hour = Integer.valueOf(alarmTime.getText().toString().split(":")[0]);
            int minute = Integer.valueOf(alarmTime.getText().toString().split(":")[1]);

            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(0);
            calendar.set(Calendar.HOUR_OF_DAY, hour);
            calendar.set(Calendar.MINUTE, minute);
            Const.waktuAlarm(type, calendar.getTimeInMillis());

            Const.ringtone(type,spinnerPuasa.getSelectedItemPosition());

            Utils.resetAlarmPuasa();
            finish();
        }
        if (id == R.id.CancelButton) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDayClick(Date dateClicked) {

    }

    @Override
    public void onMonthScroll(Date firstDayOfNewMonth) {
        Log.e("CALENDAR", "SCROLLED " + firstDayOfNewMonth.getMonth());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(firstDayOfNewMonth);
        refresh(calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR));
    }

    String[] bulan = new String[]{"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"};

    private void refresh(int month, int year) {
        calendar.removeAllEvents();
        Calendar from = Calendar.getInstance();
        from.set(Calendar.MONTH, month);
        from.set(Calendar.YEAR, year);
        from.set(Calendar.DAY_OF_MONTH, 1);
        captionCalendar.setText(bulan[month] + " " + year);

        List<Event> events = new ArrayList<>();
        while (from.get(Calendar.MONTH) == month) {
            if (type == Utils.ALARM_TYPE.PUASA_SENIN) {
                if (from.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY || from.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY) {
                    Event ev = new Event(Color.GREEN, from.getTimeInMillis());
                    events.add(ev);
                }
                from.add(Calendar.DAY_OF_MONTH, 1);
            } else if (type == Utils.ALARM_TYPE.PUASA_TENGAH_BULAN) {
                UmmalquraCalendar ummalquraCalendar = new UmmalquraCalendar();
                ummalquraCalendar.setTimeInMillis(from.getTimeInMillis());
                if (ummalquraCalendar.get(Calendar.DAY_OF_MONTH) >= 13 && ummalquraCalendar.get(Calendar.DAY_OF_MONTH) <= 15) {
                    Event ev = new Event(Color.GREEN, from.getTimeInMillis());
                    events.add(ev);
                }
                from.add(Calendar.DAY_OF_MONTH, 1);
            } else {

                int dayNow = from.get(Calendar.DAY_OF_YEAR);

                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(Const.awalDaud());
                int firstDaud = cal.get(Calendar.DAY_OF_YEAR);

                if (Math.abs(dayNow - firstDaud) % 2 == 0) {
                    Event ev = new Event(Color.GREEN, from.getTimeInMillis());
                    events.add(ev);
                }
                from.add(Calendar.DAY_OF_MONTH, 1);
            }
        }
        calendar.addEvents(events);

    }

    @OnClick(R.id.alarm_time)
    public void onClick() {
        final Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(Const.waktuAlarm(type));
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                alarmTime.setText( String.format("%02d", hour)
                        + ":" + String.format("%02d", minute));
            }
        }, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), true);
        timePickerDialog.setTitle("Waktu Pengingat");
        timePickerDialog.show();
    }
}
