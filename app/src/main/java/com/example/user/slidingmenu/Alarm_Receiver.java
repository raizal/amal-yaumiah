package com.example.user.slidingmenu;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

/**
 * Created by user on 08/06/2016.
 */
public class Alarm_Receiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context,AlarmPlayingService.class);
        i.putExtras(intent);
        context.startService(i);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_alarm_24dp)
                .setContentTitle(intent.getExtras().getString("msg")+". Ayo tunaikan target!")
                .setContentText("Tekan untuk mematikan")
                /*.addAction(R.drawable.ic_alarm_24dp, "Sudah")*/
                .setGroup("ALARM")
                .setAutoCancel(true);

        Intent intentActivity = new Intent(context, AlarmTesActivity.class);
        intentActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        intentActivity.putExtras(intent);
        context.startActivity(intentActivity);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);

        stackBuilder.addParentStack(MainActivity.class);

        stackBuilder.addNextIntent(intentActivity);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(intent.getIntExtra("id",0), PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        int m = intent.getExtras().getInt("id");
        mNotificationManager.notify(m, mBuilder.build());
        Log.v("id notif", "" + m);
    }
}
