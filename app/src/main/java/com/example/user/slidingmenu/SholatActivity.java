package com.example.user.slidingmenu;

import android.app.Activity;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

public class SholatActivity extends Activity {

    private Toolbar TopToolbar;
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sholat);
        this.context = this;

        /*TopToolbar = (Toolbar) findViewById(R.id.toolbar); // Attaching the layout to the toolbar object
        setSupportActionBar(TopToolbar);*/

        /*getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setDisplayShowHomeEnabled(true);*/

        LinearLayout SetWajib = (LinearLayout) findViewById(R.id.linier_sholatwajib);
        LinearLayout SetDhuha = (LinearLayout) findViewById(R.id.linier_sholatdhuha);
        LinearLayout SetLail = (LinearLayout) findViewById(R.id.linier_sholatlail);

        final Switch swt_wajib = (Switch) findViewById(R.id.switch_sholat_wajib);
        final Switch swt_dhuha = (Switch) findViewById(R.id.switch_sholat_dhuha);
        final Switch swt_lail = (Switch) findViewById(R.id.switch_sholat_lail);


        swt_dhuha.setChecked(Const.alarmDhuhaAktif());
        swt_lail.setChecked(Const.alarmMalamAktif());
        swt_wajib.setChecked(Const.alarmWajibAktif());

        swt_dhuha.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Const.alarmDhuhaAktif(swt_dhuha.isChecked());
                Utils.resetAlarmSholat();
            }
        });
        swt_lail.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Const.alarmMalamAktif(swt_lail.isChecked());
                Utils.resetAlarmSholat();
            }
        });
        swt_wajib.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Const.alarmWajibAktif(swt_wajib.isChecked());
                Utils.resetAlarmSholat();
            }
        });

        SetWajib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_wajib = new Intent(getApplicationContext(), SetSholatWajib.class);
                startActivity(intent_wajib);
                swt_wajib.isChecked();
            }
        });

        SetDhuha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_dhuha = new Intent(getApplicationContext(), SetSholatDhuha.class);
                startActivity(intent_dhuha);
                swt_dhuha.isChecked();
            }
        });

        SetLail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_lail = new Intent(getApplicationContext(), SetSholatLail.class);
                startActivity(intent_lail);
                swt_lail.isChecked();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        /*if (id == R.id.home) {
            Intent upIntent = NavUtils.getParentActivityIntent(this);
            if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                // This activity is NOT part of this app's task, so create a new task
                // when navigating up, with a synthesized back stack.
                TaskStackBuilder.create(this)
                        // Add all of this activity's parents to the back stack
                        .addNextIntentWithParentStack(upIntent)
                                // Navigate up to the closest parent
                        .startActivities();
            } else {
                // This activity is part of this app's task, so simply
                // navigate up to the logical parent activity.
                NavUtils.navigateUpTo(this, upIntent);
            }
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }


}
