package com.example.user.slidingmenu;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.example.user.slidingmenu.model.Alarm;
import com.example.user.slidingmenu.model.Shalat;
import com.github.msarhan.ummalqura.calendar.UmmalquraCalendar;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by raizal.pregnanta on 01/10/2016.
 */
public class Utils {

    public enum ALARM_TYPE {
        SHOLAT_WAJIB_SUBUH, SHOLAT_WAJIB_DHUHUR, SHOLAT_WAJIB_ASHAR, SHOLAT_WAJIB_MAGHRIB, SHOLAT_WAJIB_ISYA, SHOLAT_DHUHA, SHOLAT_MALAM, PUASA_SENIN, PUASA_KAMIS, PUASA_TENGAH_BULAN, PUASA_DAUD
    }

    private static final int DAYS_TO_SET = 3;

    public static String dateToDefaultFormat(Calendar calendar) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return format.format(calendar.getTime());
    }

    public static String interval(long time) {
        long milliseconds = (time - System.currentTimeMillis());
        int hours   = (int) ((milliseconds / (1000*60*60)));
        int minutes = (int) ((milliseconds / (1000*60)) % 60);
        int seconds = (int) (milliseconds / 1000) % 60 ;
        return hours+" jam "+minutes+" menit "+seconds+" detik lagi";
    }

    public static String dateToDefaultFormat(long milis) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milis);
        return dateToDefaultFormat(calendar);
    }

    public static long currentDateInMilis() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTimeInMillis();
    }

    public static long extractDateOnly(long milis) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(milis);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTimeInMillis();
    }

    public static int ringtoneRes(int index) {
        switch (index) {
            case 0:
                return R.raw.adzan;
            case 1:
                return R.raw.ibu;
            case 2:
                return R.raw.ijinkan;
            case 3:
                return R.raw.rimba;
            default:
                return R.raw.sarjana;
        }
    }

    private static void removeAlarm(List<Alarm> list) {
        AlarmManager alarmManager = (AlarmManager) Application.instance.getSystemService(Context.ALARM_SERVICE);

        for (Alarm alarm : list) {
            PendingIntent pendingIntent = PendingIntent.getService(Application.instance, (int) alarm.getId(), new Intent(), PendingIntent.FLAG_CANCEL_CURRENT);
            alarmManager.cancel(pendingIntent);
            pendingIntent.cancel();
        }
    }

    private static void removeAlarmShalat(){
        removeAlarm(Application.alarmDatabase.getAlarmSholat());
    }

    private static void removeAlarmPuasa(){
        removeAlarm(Application.alarmDatabase.getAlarmPuasa());
    }

    public static void setAlarm(Alarm alarm) {
        Intent intent = new Intent(Application.instance, Alarm_Receiver.class);
        intent.putExtra("type", alarm.getType());
        intent.putExtra("msg", alarm.getText());
        while (Application.alarmDatabase.isExists("alarm", "id", alarm.getId() + "")) {
            alarm.setId(alarm.getId() - 5);
        }
        intent.putExtra("id", alarm.getId());
        PendingIntent pendingIntent = PendingIntent.getBroadcast(Application.instance, (int) alarm.getId(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) Application.instance.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, alarm.getTime(), pendingIntent);
        Application.alarmDatabase.insertAlarm(alarm);
    }

    public static void resetAlarmSholat() {

        removeAlarmShalat();

        if (!Const.alarmWajibAktif() && !Const.alarmDhuhaAktif() && Const.alarmMalamAktif())
            return;

        Calendar calendar = Calendar.getInstance();

        int count = 0;

        for (Shalat time : Application.alarmDatabase.getJadwalShalat(Utils.currentDateInMilis())) {
            //ambil yg lebih besar sama dengan hari ini


            if (count >= DAYS_TO_SET) {
                break;
            }

            count++;

            if (Const.alarmWajibAktif()) {
                if (Const.wajibHarian(Const.WAJIB_TYPE.SUBUH))
                    if (time.getFajrTime() > System.currentTimeMillis()) {
                        setAlarm(new Alarm(Const.idFromTimeInMilis(time.getFajrTime()), time.getFajrTime(), ALARM_TYPE.SHOLAT_WAJIB_SUBUH, "Sholat Subuh"));
                    }
                if (Const.wajibHarian(Const.WAJIB_TYPE.DHUHUR))
                    if (time.getDhuhrTime() > System.currentTimeMillis()) {
                        setAlarm(new Alarm(Const.idFromTimeInMilis(time.getDhuhrTime()), time.getDhuhrTime(), ALARM_TYPE.SHOLAT_WAJIB_DHUHUR, "Sholat Dhuhur"));
                    }

                if (Const.wajibHarian(Const.WAJIB_TYPE.ASHAR))
                    if (time.getAsrTime() > System.currentTimeMillis()) {
                        setAlarm(new Alarm(Const.idFromTimeInMilis(time.getAsrTime()), time.getAsrTime(), ALARM_TYPE.SHOLAT_WAJIB_ASHAR, "Sholat Ashar"));
                    }

                if (Const.wajibHarian(Const.WAJIB_TYPE.MAGHRIB))
                    if (time.getMaghribTime() > System.currentTimeMillis()) {
                        setAlarm(new Alarm(Const.idFromTimeInMilis(time.getMaghribTime()), time.getMaghribTime(), ALARM_TYPE.SHOLAT_WAJIB_MAGHRIB, "Sholat Maghrib"));
                    }

                if (Const.wajibHarian(Const.WAJIB_TYPE.ISYA))
                    if (time.getIshaTime() > System.currentTimeMillis()) {
                        setAlarm(new Alarm(Const.idFromTimeInMilis(time.getIshaTime()), time.getIshaTime(), ALARM_TYPE.SHOLAT_WAJIB_ISYA, "Shalat Isya"));
                    }
            }

            if (Const.alarmDhuhaAktif())
                if (Const.dhuhaHarian(calendar.get(Calendar.DAY_OF_WEEK))) {
                    long dhuhaTime = time.getSunriseTime() + (1000*60*15);

                    if (dhuhaTime > System.currentTimeMillis()) {
                        setAlarm(new Alarm(Const.idFromTimeInMilis(dhuhaTime), dhuhaTime, ALARM_TYPE.SHOLAT_DHUHA, "Sholat Dhuha"));
                    }

                }

            if (Const.alarmMalamAktif())
                if (Const.malamHarian(calendar.get(Calendar.DAY_OF_WEEK))) {

                    if (time.getMidnightTime() > System.currentTimeMillis()) {
                        setAlarm(new Alarm(Const.idFromTimeInMilis(time.getMidnightTime()), time.getMidnightTime(), ALARM_TYPE.SHOLAT_MALAM, "Shalat Qiyamul Lail"));
                    }

                }


        }

    }

    //TODO setup alarm
    public static void resetAlarmPuasa() {
        removeAlarmPuasa();
        Calendar calendar = Calendar.getInstance();
        int count = 0;

        Calendar batas = Calendar.getInstance();
        batas.setTimeInMillis(Utils.extractDateOnly(batas.getTimeInMillis()));
        batas.add(Calendar.DAY_OF_MONTH, 3);

        Calendar alarmTime = Calendar.getInstance();

        while (calendar.getTimeInMillis() <= batas.getTimeInMillis()) {
            //ambil yg lebih besar sama dengan hari ini
            {
                Calendar currentDay = calendar;

                currentDay.set(Calendar.HOUR_OF_DAY, 2);

                if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
                    if (Const.alarmPuasaSeninAktif()) {

                        alarmTime.setTimeInMillis(Const.waktuAlarm(ALARM_TYPE.PUASA_SENIN));

                        currentDay.set(Calendar.HOUR_OF_DAY,alarmTime.get(Calendar.HOUR_OF_DAY));
                        currentDay.set(Calendar.MINUTE,alarmTime.get(Calendar.MINUTE));

                        if (currentDay.getTimeInMillis() > System.currentTimeMillis())
                            setAlarm(new Alarm(Const.idFromTimeInMilis(currentDay.getTimeInMillis()), currentDay.getTimeInMillis(), ALARM_TYPE.PUASA_SENIN, "Puasa Senin"));
                    }
                }

                if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY) {
                    if (Const.alarmPuasakamisAktif()) {

                        alarmTime.setTimeInMillis(Const.waktuAlarm(ALARM_TYPE.PUASA_SENIN));

                        currentDay.set(Calendar.HOUR_OF_DAY,alarmTime.get(Calendar.HOUR_OF_DAY));
                        currentDay.set(Calendar.MINUTE,alarmTime.get(Calendar.MINUTE));

                        if (currentDay.getTimeInMillis() > System.currentTimeMillis())
                            setAlarm(new Alarm(Const.idFromTimeInMilis(currentDay.getTimeInMillis()), currentDay.getTimeInMillis(), ALARM_TYPE.PUASA_KAMIS, "Puasa Kamis"));
                    }
                }

                if (Const.alarmPuasadaudAktif()) {
                    Calendar tempForCalculatingDaud = Calendar.getInstance();
                    tempForCalculatingDaud.setTimeInMillis(Math.abs(System.currentTimeMillis() - Const.awalDaud()));
                    if (tempForCalculatingDaud.get(Calendar.DAY_OF_YEAR)%2==0) {

                        alarmTime.setTimeInMillis(Const.waktuAlarm(ALARM_TYPE.PUASA_DAUD));

                        currentDay.set(Calendar.HOUR_OF_DAY,alarmTime.get(Calendar.HOUR_OF_DAY));
                        currentDay.set(Calendar.MINUTE,alarmTime.get(Calendar.MINUTE));

                        if (currentDay.getTimeInMillis() > System.currentTimeMillis())
                            setAlarm(new Alarm(Const.idFromTimeInMilis(currentDay.getTimeInMillis()), currentDay.getTimeInMillis(), ALARM_TYPE.PUASA_DAUD, "Puasa Daud"));
                    }
                }

                UmmalquraCalendar ummalquraCalendar = new UmmalquraCalendar();
                ummalquraCalendar.setTimeInMillis(calendar.getTimeInMillis());


                if (Const.alarmPuasatengahBulanAktif()) {
                    if (ummalquraCalendar.get(Calendar.DAY_OF_MONTH) >= 13 && ummalquraCalendar.get(Calendar.DAY_OF_MONTH) <= 15) {

                        alarmTime.setTimeInMillis(Const.waktuAlarm(ALARM_TYPE.PUASA_DAUD));

                        currentDay.set(Calendar.HOUR_OF_DAY,alarmTime.get(Calendar.HOUR_OF_DAY));
                        currentDay.set(Calendar.MINUTE,alarmTime.get(Calendar.MINUTE));

                        if (currentDay.getTimeInMillis() > System.currentTimeMillis())
                            setAlarm(new Alarm(Const.idFromTimeInMilis(currentDay.getTimeInMillis()), currentDay.getTimeInMillis(), ALARM_TYPE.PUASA_TENGAH_BULAN, "Puasa Ayyamul Bidh"));
                    }
                }
            }
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

    }

    public static String hadistByType(ALARM_TYPE type) {
        return "";
    }

}