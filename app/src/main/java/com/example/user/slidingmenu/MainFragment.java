package com.example.user.slidingmenu;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Switch;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment implements View.OnClickListener {


    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View layout = inflater.inflate(R.layout.fragment_main, container, false);

        //Deklarasi Button
        ImageButton mBtnSholat = (ImageButton) layout.findViewById(R.id.sholat);
        ImageButton mBtnPuasa = (ImageButton) layout.findViewById(R.id.puasa);
        ImageButton mBtnTilawah = (ImageButton) layout.findViewById(R.id.tilawah);
        ImageButton mBtnHafalan = (ImageButton) layout.findViewById(R.id.hafalan);
        ImageButton mBtnAlmatsurat = (ImageButton) layout.findViewById(R.id.almatsurat);


        mBtnSholat.setOnClickListener(this);
        mBtnPuasa.setOnClickListener(this);
        mBtnTilawah.setOnClickListener(this);
        mBtnHafalan.setOnClickListener(this);
        mBtnAlmatsurat.setOnClickListener(this);


        return layout;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sholat:
                Intent i = new Intent(getActivity(), SholatActivity.class);
                startActivity(i);

                break;
            case R.id.puasa:
                Intent j = new Intent(getActivity(), JenisPuasaActivity.class);
                startActivity(j);
                break;
            case R.id.tilawah:
                Intent k = new Intent(getActivity(), TilawahActivity.class);
                startActivity(k);
                break;
            case R.id.hafalan:
                Intent l = new Intent(getActivity(), HafalanActivity.class);
                startActivity(l);
                break;
            case R.id.almatsurat:
                Intent m = new Intent(getActivity(), AlmatsuratActivity.class);
                startActivity(m);
                break;
        }
    }
}

