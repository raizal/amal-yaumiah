package com.example.user.slidingmenu;


import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Switch;
import android.widget.TextView;

import com.example.user.slidingmenu.model.Shalat;

import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class SetSholatWajib extends AppCompatActivity {

    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.switch_sholat_subuh)
    Switch switchSholatSubuh;
    @InjectView(R.id.switch_sholat_dhuhur)
    Switch switchSholatDhuhur;
    @InjectView(R.id.switch_sholat_ashar)
    Switch switchSholatAshar;
    @InjectView(R.id.switch_sholat_maghrib)
    Switch switchSholatMaghrib;
    @InjectView(R.id.switch_sholat_isya)
    Switch switchSholatIsya;

    int[] res = new int[]{R.id.switch_sholat_subuh, R.id.switch_sholat_dhuhur, R.id.switch_sholat_ashar, R.id.switch_sholat_maghrib, R.id.switch_sholat_isya};

    int[] resText = new int[]{R.id.textSholatSubuh, R.id.textSholatDhuhur, R.id.textSholatAshar, R.id.textSholatMaghrib, R.id.textSholatIsya};
    boolean captionDone[] = new boolean[]{false, false, false, false, false};
    private long[] nextTime = new long[5];

    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_sholat_wajib);
        ButterKnife.inject(this);
        setSupportActionBar(toolbar);
        for (int i = 0; i < res.length; i++) {
            ((Switch) ButterKnife.findById(this, res[i])).setChecked(Const.wajibHarian(Const.WAJIB_TYPE.values()[i]));
        }

        List<Shalat> jadwalShalat = Application.alarmDatabase.getJadwalShalat(Utils.currentDateInMilis());

        for (Shalat s : jadwalShalat) {
            long time = Utils.currentDateInMilis();
            if (s.getTimestamp() >= time) {
                Shalat now = s;

                if (System.currentTimeMillis() <= now.getFajrTime() && !captionDone[0]) {
                    ((TextView) findViewById(resText[0])).setText("Sholat Subuh\n" + Utils.dateToDefaultFormat(now.getFajrTime()));
                    nextTime[0] = now.getFajrTime();
                    captionDone[0] = true;
                }

                if (System.currentTimeMillis() <= now.getDhuhrTime() && !captionDone[1]) {
                    ((TextView) findViewById(resText[1])).setText("Sholat Dhuhur\n" + Utils.dateToDefaultFormat(now.getDhuhrTime()));
                    nextTime[1] = now.getDhuhrTime();
                    captionDone[1] = true;
                }

                if (System.currentTimeMillis() <= now.getAsrTime() && !captionDone[2]) {
                    ((TextView) findViewById(resText[2])).setText("Sholat Ashar\n" + Utils.dateToDefaultFormat(now.getAsrTime()));
                    nextTime[2] = now.getAsrTime();
                    captionDone[2] = true;
                }

                if (System.currentTimeMillis() <= now.getMaghribTime() && !captionDone[3]) {
                    ((TextView) findViewById(resText[3])).setText("Sholat Maghrib\n" + Utils.dateToDefaultFormat(now.getMaghribTime()));
                    nextTime[3] = now.getMaghribTime();
                    captionDone[3] = true;
                }

                if (System.currentTimeMillis() <= now.getIshaTime() && !captionDone[4]) {
                    
                    nextTime[4] = now.getIshaTime();
                    captionDone[4] = true;
                }

                if (captionDone[0] && captionDone[1] && captionDone[2] && captionDone[3] && captionDone[4]) {
                    break;
                }
            }
        }
        handler.postDelayed(delayedRefresh,1000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.OkButton:
                for (int i = 0; i < res.length; i++) {
                    Const.wajibHarian(Const.WAJIB_TYPE.values()[i], ((Switch) ButterKnife.findById(this, res[i])).isChecked());
                }
                if (Const.alarmWajibAktif()) {
                    Utils.resetAlarmSholat();
                }
                finish();
                break;
            case R.id.CancelButton:
                finish();
                break;
        }
        /*if (id == R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }*/
        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }
    
    private void refresh(){
        ((TextView) findViewById(resText[0])).setText("Sholat Shubuh'\n" + Utils.dateToDefaultFormat(nextTime[0])+"\n"+Utils.interval(nextTime[0]));
        ((TextView) findViewById(resText[1])).setText("Sholat Dhuhur'\n" + Utils.dateToDefaultFormat(nextTime[1])+"\n"+Utils.interval(nextTime[1]));
        ((TextView) findViewById(resText[2])).setText("Sholat Ashar'\n" + Utils.dateToDefaultFormat(nextTime[2])+"\n"+Utils.interval(nextTime[2]));
        ((TextView) findViewById(resText[3])).setText("Sholat Maghrib'\n" + Utils.dateToDefaultFormat(nextTime[3])+"\n"+Utils.interval(nextTime[3]));
        ((TextView) findViewById(resText[4])).setText("Sholat Isya'\n" + Utils.dateToDefaultFormat(nextTime[4])+"\n"+Utils.interval(nextTime[4]));
    }

    Runnable delayedRefresh = new Runnable() {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    refresh();
                }
            });
            handler.postDelayed(this,1000);
        }
    };

    @Override
    protected void onDestroy() {
        handler.removeCallbacks(delayedRefresh);
        super.onDestroy();
    }
}