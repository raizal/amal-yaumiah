package com.example.user.slidingmenu;

import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class SystemDBHandler extends SQLiteOpenHelper {

    /*private static String DB_NAME = "id.developer.test"; // nama database*/
    private static String DB_NAME = "amal_yaumiah";
    private static int DB_VERSION = 1; // versi

    /*private static String TABLE_WORD = "word"; // nama-nama tabel
    private static String TABLE_WORD_ID = "id_word";
    private static String TABLE_WORD_WORD = "word_word";
    private static String TABLE_WORD_HINT_UK = "uk_word";
    private static String TABLE_WORD_HINT_US= "us_word";*/

    private static final String TABLE_LOGIN = "login";
    private static final String TABLE_LOGIN_STATE = "state_login";
    private static final String TABLE_LOGIN_NAME = "name_login";

    /*private static final String TABLE_HISTORY = "history";
    private static final String TABLE_HISTORY_ID = "id_history";
    private static final String TABLE_HISTORY_WORD = "word_history";
    private static final String TABLE_HISTORY_RESULT = "result_history";
    private static final String TABLE_HISTORY_DATE = "date_history";
    private static final String TABLE_HISTORY_PERCENTAGE = "percentage_history"; // sampai sini*/

    private static final String TABLE_SHALAT = "sholat";
    private static final String KOLOM_SHALAT_ID = "id_sholat";
    private static final String KOLOM_SHALAT_NAMA = "nama";
    private static final String KOLOM_SHALAT_HARI = "id_hari";

    /*public SystemDBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DB_NAME, factory, DB_VERSION);

        SQLiteDatabase db = this.getReadableDatabase();

        if(cekTableExist(db) == 0){
            insertLogin();
            insertWord(context);
        }

        db.close();
    }
*/
    public SystemDBHandler(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    /*private int cekTableExist(SQLiteDatabase db){
        Cursor c = db.rawQuery("SELECT * FROM "+TABLE_WORD, null);
        int count = c.getCount();
        c.close();
        return count;
    }*/

    // buattabel
    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE " +TABLE_SHALAT+ "(" +
                KOLOM_SHALAT_ID+ " INTEGER PRIMARY KEY, " +
                KOLOM_SHALAT_NAMA+ " TEXT, " +
                KOLOM_SHALAT_HARI+ " TEXT, " +
                ");";
        db.execSQL(query);

        query = "CREATE TABLE " +TABLE_LOGIN+" (" +
                TABLE_LOGIN_STATE+ " INTEGER," +
                TABLE_LOGIN_NAME+ " TEXT " +
                ");";
        db.execSQL(query);


    }

    // kalo upgrade
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SHALAT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOGIN);
        onCreate(db);
    }
// insert data
    private void insertLogin(){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(TABLE_LOGIN_STATE, 0);
        contentValues.put(TABLE_LOGIN_NAME, "");
        db.insert(TABLE_LOGIN, null, contentValues);
        db.close();
    }


    /*private void insertWord(Context context){
        SQLiteDatabase db = this.getWritableDatabase();

        String mCSVfile = "obj_word.csv";
        AssetManager manager = context.getAssets();
        InputStream inStream = null;

        try {
            inStream = manager.open(mCSVfile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        BufferedReader buffer = new BufferedReader(new InputStreamReader(inStream));
        String line = "";
        db.beginTransaction();
        try {
            while ((line = buffer.readLine()) != null) {
                String[] colums = line.split(",");

                ContentValues cv = new ContentValues();
                cv.put(TABLE_WORD_ID, colums[0].trim());
                cv.put(TABLE_WORD_WORD, colums[1].trim());
                cv.put(TABLE_WORD_HINT_UK, colums[2].trim());
                cv.put(TABLE_WORD_HINT_US, colums[3].trim());

                db.insert(TABLE_WORD, null, cv);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        try{
            buffer.close();
        }catch (Exception e){}
    }
*/
    public int stateLogin(){
        String query = "SELECT * FROM " +TABLE_LOGIN+ " WHERE 1";
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();

        int result;
        result = c.getInt(c.getColumnIndex(TABLE_LOGIN_STATE));

        c.close();
        db.close();

        return result;
    }

    public void updateStateLogin(int state, String name){
        SQLiteDatabase db = this.getWritableDatabase();
        int id;

        if(state == 0)
            id = 1;
        else
            id = 0;

        ContentValues values = new ContentValues();
        values.put(TABLE_LOGIN_STATE, state);
        values.put(TABLE_LOGIN_NAME, name);

        db.update(TABLE_LOGIN, values, TABLE_LOGIN_STATE + "=" + id, null);
        db.close();
    }

    public String getName(){
        String query = "SELECT * FROM " +TABLE_LOGIN+ " WHERE 1";
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        String name = cursor.getString(cursor.getColumnIndex(TABLE_LOGIN_NAME));
        cursor.close();
        db.close();

        return name;
    }
}
