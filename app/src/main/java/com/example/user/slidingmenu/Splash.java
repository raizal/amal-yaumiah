package com.example.user.slidingmenu;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.androidnetworking.error.ANError;
import com.google.android.gms.location.LocationRequest;

import java.util.Calendar;

import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Subscription;
import rx.functions.Action1;

/**
 * Created by user on 17/04/2016.
 */
public class Splash extends Activity {

    Subscription gpsSubscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen);

        final ImageView iv = (ImageView) findViewById(R.id.logo);
        final Animation an = AnimationUtils.loadAnimation(getBaseContext(), R.anim.rotate);
        final Animation an2 = AnimationUtils.loadAnimation(getBaseContext(), R.anim.abc_fade_out);

        iv.startAnimation(an);
        an.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                iv.startAnimation(an2);

                if ((Const.lng() == 0 && Const.lat() == 0) || !Const.isDataAvailable()){
                    //ambil lokasi pertama kali

                    //set ke surabaya
                    Const.lat(-7.2754665);
                    Const.lng(112.641643);

                    //download data pertama kali
                    Calendar calendar = Calendar.getInstance();
                    Const.downloadAllYear(Const.lat(), Const.lng(), new Const.DownloadJadwalListener() {
                        @Override
                        public void onComplete() {
                            finish();
                            Intent i = new Intent(getBaseContext(), MainActivity.class);
                            startActivity(i);
                        }

                        @Override
                        public void onError(ANError anError) {
                            finish();
                            Intent i = new Intent(getBaseContext(), MainActivity.class);
                            startActivity(i);
                        }
                    });

//                    initGPSListener();
                }else {
                    finish();
                    Intent i = new Intent(getBaseContext(), MainActivity.class);
                    startActivity(i);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void initGPSListener() {


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        LocationRequest request = LocationRequest.create() //standard GMS LocationRequest
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(100);

        ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(this);
        gpsSubscription = locationProvider.getUpdatedLocation(request)
                .subscribe(new Action1<Location>() {
                    @Override
                    public void call(Location location) {

                        //ketika lokasi udah ketemu
                        if (location.getLongitude() > 0 && location.getLatitude() > 0) {

                            //simpan lokasi
                            Const.lng(location.getLongitude());
                            Const.lat(location.getLatitude());

                            //matikan pencari lokasi
                            gpsSubscription.unsubscribe();

                            //ambil jadwal sholat pada bulan ini tahun ini dengan lokasi yang telah disimpan
                            Calendar calendar = Calendar.getInstance();
                            Const.downloadAllYear(location.getLatitude(), location.getLongitude(), new Const.DownloadJadwalListener() {
                                @Override
                                public void onComplete() {
                                    finish();
                                    Intent i = new Intent(getBaseContext(), MainActivity.class);
                                    startActivity(i);
                                }

                                @Override
                                public void onError(ANError anError) {
                                    finish();
                                    Intent i = new Intent(getBaseContext(), MainActivity.class);
                                    startActivity(i);
                                }
                            });

                        }
                    }
                });
    }
}